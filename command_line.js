const errorBox = document.getElementById("error-box");
const infoBox = document.getElementById("info-box");
const inputBox = document.getElementById("text-input");
const commandNames = ["save", "ses", "del", "load", "rename", "nt", "nw", "bm", "print", "csh", "lsh", "dsh", "rsh", "help", "clear", "sym", "tr"];
browser.storage.local.get("commands").then((results) => {
    if (results.commands == undefined) {
        browser.storage.local.set({"commands": commandNames});
    }
});

const history = {
    commands: [],
    index: 0
}

function historyUp() {
    if (history.index == 0) return;
    history.index--;
    inputBox.value = history.commands[history.index];
    /*const end = inputBox.value.length;
    inputBox.setSelectionRange(end, end);
    inputBox.focus();*/
}

function historyDown() {
    if (history.index == history.commands.length) return;
    if (history.index == history.commands.length - 1) {
        history.index++;
        inputBox.value = "";
    }
    else {
        history.index++;
        inputBox.value = history.commands[history.index];
        /*const end = inputBox.value.length;
        inputBox.setSelectionRange(end, end);
        inputBox.focus();*/
    }
}

function printInfo(infomsg) {
    let info = document.createElement("p");
    info.innerHTML = infomsg;
    infoBox.appendChild(info).scrollIntoView(false);
}

function printError(errormsg) {
    errorBox.innerHTML = "";
    errorBox.innerHTML = "Error: " + errormsg;
}

function Shortcut(name, tabArray = []) {
    this.name = name;
    this.tabArray = tabArray;
}

async function isCommand(string) {
    let storageReturn = await browser.storage.local.get("commands");
    if (storageReturn.commands != undefined) {
        let commandArray = storageReturn.commands;
        return commandArray.includes(string);
    }
    else {
        console.log("isCommand: results.commands is undefined!");
        return false; 
    }
}

async function parseCommand() {
    errorBox.innerHTML = ""; 

    let textInput = document.getElementById("text-input");
    let commandString = textInput.value;
    textInput.value = "";

    history.commands.push(commandString);
    if (history.commands.length > 20) history.commands.shift();
    history.index = history.commands.length;
    
    commandString = commandString.trim();
    commandString = commandString.replace(/\s+/g, " "); // replaces long whitespace with single spaces
    let fullCommand = commandString.split(' ');
    isCommand(fullCommand[0]).then((result) => {
        console.log("parseCommand: isCommand returned " + result);
        if (result) {
            var commandFunction = "cmd_" + fullCommand[0];
            var commandFlags = [];
            var commandArgs = [];
            for (let i = 1; i < fullCommand.length; i++) {
                if (fullCommand[i][0] == '-') commandFlags.push(fullCommand[i]);
                else commandArgs.push(fullCommand[i]);
            }
            console.log("Calling " + commandFunction + " with flags " + commandFlags + " and arguments " + commandArgs);
            window[commandFunction](commandFlags, commandArgs);
        }
        else {
            browser.storage.local.get("shortcuts").then((results) => {
                if (results.shortcuts != undefined) {
                    let shortcutArray = results.shortcuts;
                    for (let i = 0; i < shortcutArray.length; i++) {
                        if (shortcutArray[i].name == fullCommand[0]) {
                            let tabArray = shortcutArray[i].tabArray;
                            for (let j = 0; j < tabArray.length; j++) {
                                if (j == 0) cmd_nt("", tabArray[j]);
                                else cmd_nw("", tabArray[j]);
                            }
                            return;
                        }
                    }
                    printError("Entered command not valid");
                }
                else printError("Entered command not valid");
            });
        }
    });
}

async function addShortcut(shortcutName, shortcutTabs) {
    let shortcut = new Shortcut(shortcutName, shortcutTabs);
    let storageReturn = await browser.storage.local.get("shortcuts");
    let shortcutArray = storageReturn.shortcuts;
    if (shortcutArray == undefined) shortcutArray = [];
    shortcutArray.push(shortcut);
    browser.storage.local.set({"shortcuts": shortcutArray}).then(() => {
        printInfo("Created shortcut " + shortcutName);
    });
}

function printHelp(name, desc, flags, args) {
    let s1 = document.createElement("p");
    let s2 = document.createElement("p");
    let s3 = document.createElement("p");
    s1.innerHTML = name + ": " + desc;
    s2.innerHTML = "Flags: " + flags;
    s3.innerHTML = "Arguments: " + args;
    infoBox.appendChild(s1);
    infoBox.appendChild(s2);
    infoBox.appendChild(s3).scrollIntoView(false);
}

function cmd_help(flags, args) { 
    let p1 = document.createElement("p");
    p1.innerHTML = "help for browser command line interface:"

    let list = document.createElement("ul");
    let li1 = document.createElement("li");
    li1.innerHTML =  "Syntax: [command] [-flag(s)] [argument(s)]";
    let li2 = document.createElement("li");
    li2.innerHTML = "All flags begin with a '-' character and are exactly one letter long.";
    let li3 = document.createElement("li");
    li3.innerHTML = "A command may take zero or more arguments. Excess arguments will be ignored, but will not cause an error."
    let li4 = document.createElement("li");
    li4.innerHTML = "To see help for a given command, use [command] -h";

    let p2 = document.createElement("p");
    let commandListString = "List of commands: "
    for (let i = 0; i < commandNames.length; i++) {
        commandListString += (i > 0 ? ", " : " ") + commandNames[i];
    }
    p2.innerHTML = commandListString;
    infoBox.appendChild(p1);
    list.appendChild(li1).appendChild(li2).appendChild(li3).appendChild(li4);
    infoBox.appendChild(list);
    infoBox.appendChild(p2).scrollIntoView(false);
}

function cmd_save(flags, args) { // command: save current session
    if (flags.includes("-h")) {
        printHelp("save",
        "Saves current session (all open windows and tabs).",
        "none",
        "Optional arguments are concatenated to form the name of the saved session. By default, the session name is its time of creation.");
        return;
    }
    browser.windows.getAll({populate: true}).then((windowArray) => {
        let sessionTabsUrls = new Array();
        for (let i = 0; i < windowArray.length; i++) {
            let windowTabArray = windowArray[i].tabs;
            let windowTabsUrls = new Array();
            for (let j = 0; j < windowTabArray.length; j++) {
                let url = windowTabArray[j].url;
                windowTabsUrls.push(url);
            }
            sessionTabsUrls.push(windowTabsUrls);
        }
        
        var sessionArray;
        browser.storage.local.get("sessions").then((results) => {
            let timestamp = new Date();
            let myDate = timestamp.getDate() + "." + (timestamp.getMonth() + 1) + ". " + (timestamp.getHours() < 10 ? "0" : "") + timestamp.getHours() + ":" + (timestamp.getMinutes() < 10 ? "0" : "") + timestamp.getMinutes();
            let sesName = (args.length == 0 ? myDate : args.join(' '));
            let sessionObj = {
                id: 1,
                name: sesName,
                tabs: sessionTabsUrls,
                time: myDate
            }
            if (results.sessions != undefined) {
                sessionArray = results.sessions;
                let lastObjId = sessionArray.at(-1).id;
                sessionObj.id = lastObjId + 1;
            }
            else sessionArray = new Array();
            sessionArray.push(sessionObj);
            browser.storage.local.set({"sessions": sessionArray}).then(() => {
                printInfo("Saved session as " + sesName + ".");
            });
        });
    });
}

function cmd_ses(flags, args) { // command: list saved sessions
    if (flags.includes("-h")) {
        printHelp("ses",
        "Lists saved sessions.",
        "-l: additionally lists URLs with each session",
        "none");
        return;
    }
    browser.storage.local.get("sessions").then((results) => {
        if (results.sessions != undefined) {
            let SessionArray = results.sessions;
            for (let i = 0; i < SessionArray.length; i++) {
                let tabUrlSpan = document.createElement("span");
                tabUrlSpan.innerHTML = SessionArray[i].id + " - " + SessionArray[i].name + (flags.includes("-l") ? ": " + SessionArray[i].tabs : "")  + " - " + SessionArray[i].time;
                infoBox.appendChild(tabUrlSpan);
                if (i < SessionArray.length - 1) infoBox.appendChild(document.createElement("br"));
                else infoBox.appendChild(document.createElement("br")).scrollIntoView(false);
            }
        }
        else {
            printInfo("No sessions saved.");
        }
    });
}

function cmd_del(flags, args) { // command: delete saved session
    if (flags.includes("-h")) {
        printHelp("del",
        "Deletes session by ID.",
        "none",
        "Argument must be a valid session ID, found by using the \'ses\' command.");
        return;
    }
    if (args.length < 1) {
        printError("Not enough arguments, expected 1");
        return;
    }
    let idToDelete = parseInt(args[0], 10);
    if (isNaN(idToDelete)) {
        printError("Invalid id");
        return;
    }
    browser.storage.local.get("sessions").then((results) => {
        if (results.sessions != undefined) {
            let sessionArray = results.sessions;
            let deletedSession = false;
            let deletedIndex = undefined;
            for (let i = 0; i < sessionArray.length; i++) {
                if (sessionArray[i].id == idToDelete) {
                    sessionArray.splice(i, 1);
                    deletedSession = true;
                    deletedIndex = i;
                }
            }
            if (deletedSession) {
                for (let i = deletedIndex; i < sessionArray.length; i++) {
                    sessionArray[i].id--;
                }
                printInfo("Deleted session " + idToDelete + ".");
            }
            else {
                printError("Session " + idToDelete + " does not exist");
            }
            if (sessionArray.length > 0) browser.storage.local.set({"sessions": sessionArray});
            else browser.storage.local.remove("sessions");
        }
        else {
            printError("Session " + idToDelete + " does not exist");
        }
    });
}

function cmd_load(flags, args) { // command: load saved session
    if (flags.includes("-h")) {
        printHelp("load",
        "Loads session by ID",
        "none",
        "Argument must be a valid session ID, found by using the \'ses\' command.");
        return;
    }
    if (args.length < 1) {
        printError("Not enough arguments, expected 1");
        return;
    }
    let idToLoad = parseInt(args[0], 10);
    if (isNaN(idToLoad)) {
        printError("Invalid id");
        return;
    }
    browser.storage.local.get("sessions").then((results) => {
        if (results.sessions != undefined) {
            var sessionObj;
            let sessionArray = results.sessions;
            for (let i = 0; i < sessionArray.length; i++) {
                if (sessionArray[i].id == idToLoad) {
                    sessionObj = sessionArray[i];
                    break;
                } 
            }
            if (sessionObj != undefined) {
                let windows = sessionObj.tabs;
                for (let i = 0; i < windows.length; i++) {
                    cmd_nw("", windows[i]);
                }
            }
            else {
                printError("Session " + idToLoad + " does not exist");
            }
        }
        else {
            printError("No saved sessions");
        }
    });
}

function cmd_rename(flags, args) { // command: rename saved session
    if (flags.includes("-h")) {
        printHelp("rename",
        "Renames session by ID",
        "none",
        "First argument must be a valid session ID, found by using the \'ses\' command. All additional arguments are concatenated to form the new session name.");
        return;
    }
    if (args.length < 2) {
        printError("Not enough arguments, expected 2");
        return;
    }
    let idToRename = parseInt(args[0], 10);
    if (isNaN(idToRename)) {
        printError("Invalid id");
        return;
    }
    browser.storage.local.get("sessions").then((results) => {
        if (results.sessions != undefined) {
            var sessionObj;
            let sessionArray = results.sessions;
            if (idToRename > sessionArray.length) {
                printError("Session " + idToRename + " does not exist");
                return;
            }
            let oldName = sessionArray[idToRename - 1].name;
            let newName = args.slice(1).join(' ');
            sessionArray[idToRename - 1].name = newName;
            browser.storage.local.set({"sessions": sessionArray}).then(() => {
                printInfo("Renamed session " + oldName + " to " + newName + ".");
            });
        }
        else {
            printError("Session " + idToRename + " does not exist");
        }
    });
}

function cmd_nt(flags, args) { // command: new tab
    if (flags.includes("-h")) {
        printHelp("nt",
        "Opens new tab(s).",
        "-s: sets command to search mode",
        "Without flags, arguments must be one or more space-separated URLs to be opened. With the -s flag, arguments are concatenated to form a query which will be searched using the browser's default search engine in a new tab.");
        return;
    }
    if (flags.includes("-s")) {
        browser.search.search({query: args.join(' ')});
    }
    else {
        var arg;
        for (arg of args) {
            browser.tabs.create({url: arg});
        }
    }
}

function cmd_nw(flags, args) { // command: new window
    if (flags.includes("-h")) {
        printHelp("nw",
        "Opens new window.",
        "-p: opens private window | -s: sets command to search mode",
        "Without flags, arguments must be one or more space-separated URLs to be opened. With the -s flag, arguments are concatenated to form a query which will be searched using the browser's default search engine in a new window.");
        return;
    }
    if (flags.includes("-p")) {
        let incognitoAllowed = browser.extension.isAllowedIncognitoAccess();
        incognitoAllowed.then((result) => {
            if (!result) {
                printError("The extension does not have permission to run in incognito tabs");
            }
        });
    }

    browser.windows.create({focused: false, incognito: (flags.includes("-p") ? true : false)}).then((windowInfo) => {
        let windowId = windowInfo.id;
        if (flags.includes("-s")) {
            let searchTab = browser.tabs.create({windowId: windowId});
            searchTab.then((tabInfo) => {
                let tabId = tabInfo.id;
                browser.search.search({query: args.join(' '), tabId: tabId});
            });
        }
        else {
            var arg;
            for (arg of args) {
                browser.tabs.create({url: arg, windowId: windowId});
            }
        }
        browser.windows.get(windowId, {populate: true}).then((windowInfo2) => {
            let tabs = windowInfo2.tabs;
            browser.tabs.remove(tabs[0]["id"]);
        });
        browser.windows.update(windowId, {focused: true});
    });
}

function cmd_bm(flags, args) { // command: bookmark
    if (flags.includes("-h")) {
        printHelp("bm",
        "Bookmarks current tab.",
        "none",
        "Optional arguments are concatenated to form the name of the created bookmark. By default the name is the bookmarked tab's URL.");
        return;
    }
    browser.tabs.query({active: true}).then((activeTab) => {
        let currentUrl = activeTab[0].url;
        let bmName = (args.length != 0 ? args.join(' ') : currentUrl);
        browser.bookmarks.create({
            title: bmName,
            url: currentUrl
        });
        printInfo("Created bookmark " + bmName + ".");
    });
}

function cmd_print(flags, args) { // command: print current tab
    if (flags.includes("-h")) {
        printHelp("print",
        "Opens the browser's printing dialog for the current page.",
        "none",
        "none");
        return;
    }
    browser.tabs.print();
}

function cmd_csh(flags, args) { // command: create shortcut
    if (flags.includes("-h")) {
        printHelp("csh",
        "Creates shortcut.",
        "-s: creates shortcut from saved session | -u: creates shortcut from given URLs",
        "The first argument must be the name of the shortcut, which may not contain spaces. With the -s flag, the second argument must be a valid session ID, found by using the \'ses\' command. With the -u flag, the following arguments must be one or more space-separated URLs to be opened as tabs. These tabs may be split into windows by separating URLs with the \';\' character.");
        return;
    }
    if (flags.length != 1) {
        printError("Wrong number of flags, expected 1");
        return;
    }
    if (args.length < 2) {
        printError("Not enough arguments, expected 2");
        return;
    }
    if (flags.includes("-s")) { // create shortcut from saved session
        let sesId = parseInt(args[1], 10);
        if (isNaN(sesId)) {
            printError("Invalid session id");
            return;
        }
        let shortcutName = args[0];
        browser.storage.local.get("sessions").then((results) => {
            if (results.sessions != undefined) {
                var tabArray;
                let sessionArray = results.sessions;
                if (sesId > sessionArray.length) {
                    printError("Invalid session id");
                    return;
                }
                for (ses of sessionArray) {
                    if (ses.id == sesId) {
                        tabArray = ses.tabs;
                        break;
                    }
                }
                addShortcut(shortcutName, tabArray);
            }
            else {
                printError("Session " + sesId + " does not exist");
            }
        });
    }
    else if (flags.includes("-u")) { // create shortcut from user provided list of URLs
        let shortcutName = args[0];
        let argGroupArray = args.slice(1).join(' ').split(';');
        console.log("shortcutName: " + shortcutName + "\nargGroupArray: " + argGroupArray);
        var tabArray = [];
        for (urls of argGroupArray) {
            urls = urls.trim();
            urls = urls.replace(/\s+/g, " "); // replaces longer spaces with single spaces, necessary for correct splitting in next line
            let windowTabs = urls.split(' ');
            tabArray.push(windowTabs);
        }
        console.log(tabArray);
        addShortcut(shortcutName, tabArray);
    }
    else {
        printError("Invalid flag");
    }
}

function cmd_lsh(flags, args) { // command: list shortcuts
    if (flags.includes("-h")) {
        printHelp("lsh",
        "Lists defined shortcuts.",
        "-l: additionally lists target URLs with each shortcut",
        "none");
        return;
    }
    browser.storage.local.get("shortcuts").then((results) => {
        if (results.shortcuts == undefined) {
            printInfo("No defined shortcuts");
            return;
        }
        else {
            let shortcutArray = results.shortcuts;
            for (let i = 0; i < shortcutArray.length; i++) {
                let span = document.createElement("span");
                let shortcutString = (i + 1) + ": " + shortcutArray[i].name;
                if (flags.includes("-l")) {
                    let tabArray = shortcutArray[i].tabArray;
                    let urlString = "";
                    for (let j = 0; j < tabArray.length; j++) {
                        let tabs = tabArray[j];
                        for (let k = 0; k < tabs.length; k++) {
                            urlString += ((j == 0 && k == 0) ? "" : ", ") + tabs[k];
                        }
                    }
                    shortcutString += " - " + urlString;
                }
                span.innerHTML = shortcutString;
                infoBox.appendChild(span);
                if (i < shortcutArray.length - 1) infoBox.appendChild(document.createElement("br"));
                else infoBox.appendChild(document.createElement("br")).scrollIntoView(false);
            }
        }
    });
}

function cmd_dsh(flags, args) { // command: delete shortcut
    if (flags.includes("-h")) {
        printHelp("dsh",
        "Deletes shortcut by ID.",
        "none",
        "Argument must be a valid shortcut ID, found by using the \'lsh\' command.");
        return;
    }
    if (args.length < 1) {
        printError("Not enough arguments, expected 1");
        return;
    }
    browser.storage.local.get("shortcuts").then((results) => {
        if (results.shortcuts != undefined) {
            let shortcutArray = results.shortcuts;
            for (let i = 0; i < shortcutArray.length; i++) {
                if (args.includes((i+1).toString())) {
                    shortcutArray.splice(i, 1);
                    i--;
                }
            }
            printInfo("Deleted shortcut(s).");
            if (shortcutArray.length > 0) browser.storage.local.set({"shortcuts": shortcutArray});
            else browser.storage.local.remove("shortcuts");
        }
        else {
            printError("No shortcuts defined");
        }
    });
}

function cmd_rsh(flags, args) { // command: rename shortcut
    if (flags.includes("-h")) {
        printHelp("rsh",
        "Renames shortcut by ID.",
        "none",
        "First argument must be a valid shortcut ID, found by using the \'lsh\' command. Second argument must be the new shortcut name, which may not contain spaces.");
        return;
    }
    if (args.length < 2) {
        printError("Not enough arguments, expected 2");
        return;
    }
    let idToRename = parseInt(args[0]);
    if (isNaN(idToRename)) {
        printError("Invalid id");
        return;
    }
    browser.storage.local.get("shortcuts").then((results) => {
        if (results.shortcuts != undefined) {
            let shortcutArray = results.shortcuts;
            if (idToRename > shortcutArray.length || idToRename < 1) {
                printError("Invalid id");
                return;
            }
            let oldName = shortcutArray[idToRename - 1].name;
            let newName = args[1];
            shortcutArray[idToRename - 1].name = newName;
            browser.storage.local.set({"shortcuts": shortcutArray});
            printInfo("Renamed shortcut " + oldName + " to " + newName + ".");
        }
        else {
            printError("No shortcuts defined");
        }
    });
}

function cmd_clear(flags, args) {
    if (flags.includes("-h")) {
        printHelp("clear",
        "Clears output field.",
        "none",
        "none");
        return;
    }
    infoBox.innerHTML = "";
}

function cmd_sym(flags, args) {
    if (flags.includes("-h")) {
        printHelp("sym",
        "Manages the symbol inserter function.",
        "none",
        "Without arguments, the current state of the function is returned. The argument \'1\' enables it, the argument \'0\' disables it. A change in state forces the tab to reset.");
        return;
    }
    if (args[0] === "1") {
        browser.storage.local.set({"symbolInserterState": true}).then(() => {
            browser.tabs.reload();
            printInfo("Enabled symbol inserter");
        });
    }
    else if (args[0] === "0") {
        browser.storage.local.set({"symbolInserterState": false}).then(() => {
            browser.tabs.reload();
            printInfo("Disabled symbol inserter");
        });
    }
    else {
        browser.storage.local.get("symbolInserterState").then((results) => {
            printInfo("Symbol inserter: " + (results.symbolInserterState ? "enabled" : "disabled"));
        });
    }
}

function cmd_tr(flags, args) {
    if (flags.includes("-h")) {
        printHelp("tr",
        "Opens the translation menu.",
        "none",
        "none");
        return;
    }
    browser.tabs.create({url: browser.runtime.getURL("index.html")});
}

document.getElementById("submit-button").addEventListener('click', parseCommand);
document.getElementById("text-input").addEventListener('keydown', (event) => {
    if (event.code == "Enter") parseCommand();
    else if (event.code == "ArrowUp") historyUp();
    else if (event.code == "ArrowDown") historyDown();
});
document.getElementById("text-input").focus();
